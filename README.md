### Hi there 👋

- 🔭 I’m currently working on HPC & Cloud
- 🌱 I’m currently learning Rust
- 👯 I’m looking to collaborate on HPC, Ansible, Rocky Linux
- 🤔 I’m looking for help with HPC, Rocky Linux
- 💬 Ask me about OS Deployment and Automation
- 📫 How to reach me: hsmalley@protonmail.com or 146.52 MHZ
- 😄 Pronouns: He/Him
- ⚡ Fun fact: I solved a rubik's cube by accident once..
